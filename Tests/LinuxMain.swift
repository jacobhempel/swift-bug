import XCTest

import bugTests

var tests = [XCTestCaseEntry]()
tests += bugTests.allTests()
XCTMain(tests)
